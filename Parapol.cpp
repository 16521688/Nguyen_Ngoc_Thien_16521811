#include "Parapol.h"
#include "line.h"
void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int x = 0;
	int y = 0;
	int p = 1 - A;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			y++;
			p += 2 * x + 3 - 2 * A;
		}
		x++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}

	//Area 2
	p = 2 * A - 1;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (y+yc<600)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			x++;
			p += 4 * A - 4 * x - 4;
		}
		y++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int x = 0;
	int y = 0;
	int p = -1 - A;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			y--;
			p += 2 * x + 3 - 2 * A;
		}
		x++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}

	//Area 2
	p = 2 * A - 1;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw2Points(xc, yc, x, y, ren);
	while (y + yc > 0)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			x++;
			p += 4 * A - 4 * x - 4;
		}
		y--;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw2Points(xc, yc, x, y, ren);
	}
}
