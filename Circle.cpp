#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	//7 points
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, y + xc, x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, x + yc);
	SDL_RenderDrawPoint(ren, y + xc, -x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, -x + yc);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int p = 3 - R*sqrt(2);
	int x = -R / sqrt(2);
	int y = R / sqrt(2);
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw8Points(xc, yc, x, y, ren);
	float const1 = -R / (sqrt(2));
	while (y > 0) {
		if (p >= 0) {
			p += -4 * y + 6;
		}
		else {
			x = x - 1;
			p += -4 * x - 4 * y + 10;
		}
		y = y - 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int p = 1 - R;
	int x = R;
	int y = 0;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw8Points(xc, yc, x, y, ren);
	float const1 = -R / (sqrt(2));
	while (y > const1) {
		if (p <= 0) {
			p += -2 * y + 3;
		}
		else {
			x = x - 1;
			p += -2 * x - 2 * y + 5;
		}
		y = y - 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw8Points(xc, yc, x, y, ren);
	}
}
